CC = gcc
OUT = mydiz
BIN_DIR = bin
SRC_DIR = src
OBJS = main.o list.o malloc.o block_file.o di_file.o dinode_info.o
OBJS_DIR = $(addprefix $(BIN_DIR)/,$(OBJS))
CFLAGS = -c -o $@ -Wall


all: $(OBJS_DIR)
	$(CC) -o $(BIN_DIR)/$(OUT) $(OBJS_DIR)
	
$(BIN_DIR)/main.o: $(SRC_DIR)/main.c
	$(CC) $(CFLAGS) $<

$(BIN_DIR)/list.o: $(SRC_DIR)/data_structures/list.c $(SRC_DIR)/data_structures/list.h
	$(CC) $(CFLAGS) $<

$(BIN_DIR)/dinode_info.o: $(SRC_DIR)/di_structure/dinode_info.c $(SRC_DIR)/di_structure/dinode_info.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/di_file.o: $(SRC_DIR)/di_structure/di_file.c $(SRC_DIR)/di_structure/di_file.h 
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/malloc.o: $(SRC_DIR)/malloc/malloc.c
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/block_file.o: $(SRC_DIR)/block_structure/block_file.c $(SRC_DIR)/block_structure/block_file.h
	$(CC) $(CFLAGS) $<
	
$(OBJS_DIR): | $(BIN_DIR)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)
	
clean:	
	rm -f $(BIN_DIR)/$(OUT) $(OBJS_DIR)
