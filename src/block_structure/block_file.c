#include "block_file.h"


typedef struct BF {
    List block_list;
    int file_descriptor;
    int current_block;
    void *block;
} BF;

BF *block_file;



void BF_init() {
    block_file = c_malloc(sizeof(BF));

    block_file->file_descriptor = -1;
    List_create(&block_file->block_list);
}


int BF_createFile(char *file_name) {
    //create and open a new file
    block_file->file_descriptor = open(file_name, O_WRONLY|O_CREAT|O_TRUNC, PERMS);
    return BF_SUCCESS;
}


int BF_openFile(char *file_name) {
    //open and load all the blocks from the file
    block_file->file_descriptor = open(file_name, O_RDWR, PERMS);
    if (block_file->file_descriptor == -1)
        return BFE_OPENFILE;
    else {
        void *block = c_malloc(BLOCK_SIZE);
        while (read(block_file->file_descriptor, block, BLOCK_SIZE) > 0) {
            List_addLast(block_file->block_list, block);
            block = c_malloc(BLOCK_SIZE);
        }
        free(block);

        return BF_SUCCESS;
    }
}


int BF_getBlockCounter() {
    //return the number of blocks
    return List_returnSize(block_file->block_list);
}


int BF_allocateBlock() {
    //create and initialize a new block
    void *new_block = c_malloc(BLOCK_SIZE);
    memset(new_block, '0', BLOCK_SIZE);
    List_addLast(block_file->block_list, new_block);
    return BF_SUCCESS;
}


int BF_readBlock(int block_number, void **block) {
    void *data;
    int counter = 0;
    Iterator it;
    List_initIterator(block_file->block_list, &it);

    while ((data = List_returnValue(&it)) != NULL) {
        if (counter == block_number) {
            *block = data;
            block_file->block = data;
            block_file->current_block = counter;
            return BF_SUCCESS;
        }
        counter++;
    }

    return BFE_NOTFOUND;
}


void BF_writeBlock() {
    int fd = block_file->file_descriptor, offset = block_file->current_block * BLOCK_SIZE;
    //move cursor to current block
    lseek(fd, offset, SEEK_SET);
    write(fd, block_file->block, BLOCK_SIZE);
}


int BF_close() {
    int i, size = List_returnSize(block_file->block_list);
    void *block;

    //write all blocks
    for (i = 0; i < size; i++) {
        BF_readBlock(i, &block);
        BF_writeBlock();
    }

    List_destroy(&block_file->block_list);

    //close file
    int error = close(block_file->file_descriptor);
    free(block_file);
    if (error == 0)
        return BF_SUCCESS;
    else
        return BFE_CLOSEFILE;
}
