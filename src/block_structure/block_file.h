#ifndef _BLOCK_FILE_H
#define _BLOCK_FILE_H

#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "../malloc/malloc.h"
#include "../data_structures/list.h"

#define BLOCK_SIZE 2048
#define PERMS 0755

#define BF_SUCCESS 0
#define BFE_NOTFOUND -1
#define BFE_OPENFILE -2
#define BFE_CLOSEFILE -3


void BF_init();

int BF_createFile(char *file_name);

int BF_openFile(char *file_name);

int BF_getBlockCounter();

int BF_allocateBlock();

int BF_readBlock(int block_number, void **block);

int BF_close();

#endif //_BLOCK_FILE_H
