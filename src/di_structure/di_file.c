#include "di_file.h"


typedef struct DIfile {
    int start_block;
    List dinode_info_list;
    List dinode_list;
} DIfile;



void writeList(List lst, int *current_block, int *current_byte, size_t struct_size);

void loadList(List lst, int *current_block, int *current_byte, int nodes_count, size_t struct_size);


DIfilePtr DIfile_create() {
    DIfilePtr new_di_file = c_malloc(sizeof(DIfile));

    new_di_file->start_block = 1;
    List_create(&new_di_file->dinode_info_list);
    List_create(&new_di_file->dinode_list);

    return new_di_file;
}


QueryAnswer DIfile_query(DIfilePtr di_file, char *file) {
    DInodeInfo *dinode_info;
    Iterator it;
    List_initIterator(di_file->dinode_info_list, &it);
    while ((dinode_info = List_returnValue(&it)) != NULL) {
        if (DInodeInfo_query(dinode_info, file) == yes)
            return yes;
    }
    return no;
}


int DIfile_add(DIfilePtr di_file, char *dir_file, int previous_dir_pointer, FileState file_state) {
    DIR* dir_ptr;
    struct dirent *direntp;
    struct  stat  statbuf;

    if (lstat(dir_file, &statbuf) ==  -1) {
        perror("Failed  to get  file  status first");
        exit(EXIT_FAILURE);
    }
    else {
        int current_pointer;
        DInodeInfo *dinode_info = DInodeInfo_create(dir_file, file_state);
        current_pointer = List_returnSize(di_file->dinode_info_list);
        List_addLast(di_file->dinode_info_list, dinode_info);

        if ((statbuf.st_mode & S_IFMT) == S_IFDIR) {       //directory
            int info_node_pointer;
            if ((dir_ptr = opendir(dir_file)) == NULL) {
                fprintf(stderr, "cannot  open %s \n", dir_file);
                exit(EXIT_FAILURE);
            }
            else {
                DInodeInfo_addEntry(dinode_info, ".", current_pointer,
                                    di_file->dinode_list);
                if (previous_dir_pointer == -1)               //root directory
                    DInodeInfo_addEntry(dinode_info, "..", current_pointer,
                                        di_file->dinode_list);
                else
                    DInodeInfo_addEntry(dinode_info, "..", previous_dir_pointer,
                                        di_file->dinode_list);

                chdir(dir_file);
                while ((direntp = readdir(dir_ptr)) != NULL ) {
                    if (strcmp("..",direntp->d_name) != 0 && strcmp(".",direntp->d_name) != 0) {
                        info_node_pointer = DIfile_add(di_file, direntp->d_name, current_pointer, file_state);
                        DInodeInfo_addEntry(dinode_info, direntp->d_name, info_node_pointer,
                                di_file->dinode_list);
                    }
                }
                closedir(dir_ptr);
            }
            chdir("..");
            return current_pointer;
        }
        else if ((statbuf.st_mode & S_IFMT) == S_IFREG) {    //file
            DInodeInfo_copyFile(dinode_info, &di_file->start_block);
            return current_pointer;
        }
        else if ((statbuf.st_mode & S_IFMT) == S_IFLNK) {     //link
            return current_pointer;
        }
        else
            return -1;
    }
}


void writeList(List lst, int *current_block, int *current_byte, size_t struct_size) {
    void *block, *value;
    Iterator it;
    List_initIterator(lst, &it);

    BF_readBlock(*current_block, &block);
    block += *current_byte;
    while ((value = List_returnValue(&it)) != NULL) {
        if ((*current_byte + struct_size) > BLOCK_SIZE) { //current block is full
            (*current_block)++;
            if (*current_block >= BF_getBlockCounter())
                BF_allocateBlock();
            *current_byte = 0;
            BF_readBlock(*current_block, &block);
        }

        memcpy(block, value, struct_size);
        *current_byte += struct_size;
        block += struct_size;
    }
}


void DIfile_writeToFile(DIfilePtr di_file) {
    int current_byte = 0, current_block = di_file->start_block;
    void *block;

    if (di_file->start_block >= BF_getBlockCounter())
        BF_allocateBlock();

    //writing dinode info nodes
    writeList(di_file->dinode_info_list, &current_block, &current_byte, sizeof(DInodeInfo));
    //write dinode nodes
    writeList(di_file->dinode_list, &current_block, &current_byte, sizeof(DInode));

    //write header
    BF_readBlock(0, &block);
    //write first block id metadata
    memcpy(block, &di_file->start_block, sizeof(int));
    //write number of dinode_info nodes
    block += sizeof(int);
    int count = List_returnSize(di_file->dinode_info_list);
    memcpy(block, &count, sizeof(int));
    //write number of dinode nodes
    count = List_returnSize(di_file->dinode_list);
    block += sizeof(int);
    memcpy(block, &count, sizeof(int));
}


void loadList(List lst, int *current_block, int *current_byte, int nodes_count, size_t struct_size) {
    void *block, *value;
    int nodes_loaded_count = 0;
    while (nodes_loaded_count < nodes_count) {
        BF_readBlock(*current_block, &block);
        block += *current_byte;
        while ((*current_byte + struct_size) <= BLOCK_SIZE) {
            value = c_malloc(struct_size);
            memcpy(value, block, struct_size);
            List_addLast(lst, value);
            nodes_loaded_count++;
            *current_byte += struct_size;
            block += struct_size;
            if (nodes_loaded_count == nodes_count)
                return;
        }
        (*current_block)++;
        *current_byte = 0;
    }
}


void DIfile_loadFromFile(DIfilePtr di_file) {
    int dinode_info_count, dinode_count, current_byte = 0, current_block;
    void *block;

    //read header
    BF_readBlock(0, &block);
    //read first block id of metadata
    memcpy(&current_block, block, sizeof(int));
    //read number of dinode_info_nodes
    block += sizeof(int);
    memcpy(&dinode_info_count, block, sizeof(int));
    //read number of dinode nodes
    block += sizeof(int);
    memcpy(&dinode_count, block, sizeof(int));

    di_file->start_block = current_block;
    //load dinode_info nodes
    loadList(di_file->dinode_info_list, &current_block, &current_byte,
             dinode_info_count, sizeof(DInodeInfo));

    //load dinode nodes
    loadList(di_file->dinode_list, &current_block, &current_byte,
             dinode_count, sizeof(DInode));
}


void DIfile_extractAll(DIfilePtr di_file) {
    int i, dinode_info_count = List_returnSize(di_file->dinode_info_list);
    int *visited_nodes = c_malloc(sizeof(int) * dinode_info_count);
    for (i = 0; i < dinode_info_count; i++)
        visited_nodes[i] = 0;

    Iterator it;
    List_initIterator(di_file->dinode_info_list, &it);

    int dinode_id;
    DInodeInfo *dinode_info;
    DInode *dinode;

    while ((dinode_info = List_returnValue(&it)) != NULL) {
        if ((dinode_info->file_mode & S_IFMT) == S_IFDIR) {   //directory
            dinode_id = dinode_info->dinode_pointers[0];
            dinode = List_find(di_file->dinode_list, dinode_id);
            //find if it is root directory ( . == ..)
            if (dinode->entries[0].dinode_info_pointer == dinode->entries[1].dinode_info_pointer) {
                DInodeInfo_extractDirectory(dinode_info, di_file->dinode_info_list, di_file->dinode_list, visited_nodes);
                visited_nodes[dinode->entries[0].dinode_info_pointer] = 1;
            }
        }
    }

    //extract remaining files
    List_initIterator(di_file->dinode_info_list, &it);
    for (i = 0; (dinode_info = List_returnValue(&it)) != NULL; i++) {
        if (visited_nodes[i] == 0)
            DInodeInfo_extract(dinode_info);
    }
    free(visited_nodes);
}


void DIfile_printHierarchy(DIfilePtr di_file) {
    DInodeInfo *dinode_info;
    DInode *dinode;
    int dinode_id;

    int i, dinode_info_count = List_returnSize(di_file->dinode_info_list);

    //create and initialize visited nodes array
    int *visited_nodes = c_malloc(sizeof(int) * dinode_info_count);
    for (i = 0; i < dinode_info_count; i++)
        visited_nodes[i] = 0;

    //iterate throught all dinode_info list
    Iterator dinode_info_it;
    List_initIterator(di_file->dinode_info_list, &dinode_info_it);
    while ((dinode_info = List_returnValue(&dinode_info_it)) != NULL) {
        //print only root directories
        if ((dinode_info->file_mode & S_IFMT) == S_IFDIR) {   //directory
            dinode_id = dinode_info->dinode_pointers[0];
            dinode = List_find(di_file->dinode_list, dinode_id);
            //find if it is root directory ( . == ..)
            if (dinode->entries[0].dinode_info_pointer == dinode->entries[1].dinode_info_pointer) {
                printf("%s\n", dinode_info->file_dir_name);
                visited_nodes[dinode->entries[0].dinode_info_pointer] = 1;
                printDirHierarchy(dinode_info, di_file->dinode_info_list, di_file->dinode_list, visited_nodes);
            }
        }
    }

    //print remaining files
    Iterator it;
    List_initIterator(di_file->dinode_info_list, &it);
    for (i = 0; (dinode_info = List_returnValue(&it)) != NULL; i++) {
        if (visited_nodes[i] == 0)
            printf("%s\n", dinode_info->file_dir_name);
    }
    free(visited_nodes);
}


void DIfile_printMetadata(DIfilePtr di_file) {
    int i = 0;
    Iterator it;
    List_initIterator(di_file->dinode_info_list, &it);

    DInodeInfo *dinode_info;
    while ((dinode_info = List_returnValue(&it)) != NULL) {
        printf("id: %2d | ", i++);
        DInodeInfo_print(*dinode_info);
    }

    printf("-------------------------------------------------------------------------\n");

    List_initIterator(di_file->dinode_info_list, &it);
    while ((dinode_info = List_returnValue(&it)) != NULL)
        DInode_print(*dinode_info, di_file->dinode_list);
}


void DIfile_destroy(DIfilePtr *di_file) {
    List_destroy(&(*di_file)->dinode_info_list);
    List_destroy(&(*di_file)->dinode_list);
    free(*di_file);
    *di_file = NULL;
}
