#include "dinode_info.h"


char *modes[] = {"---","--x","-w-","-wx","r--","r-x","rw-","rwx"};


int compressFile(char *file);

int decompressFile(char *file);

void printIndented(char *name, int depth);



DInodeInfo* DInodeInfo_create(char *file, FileState file_state) {
    DInodeInfo *new_dinode_info = c_malloc(sizeof(DInodeInfo));

    int i;
    //initialize dinode pointers
    for (i = 0; i < POINTERS_COUNT; i++)
        new_dinode_info->dinode_pointers[i] = -1;

    struct stat statbuf;
    lstat(file, &statbuf);
    new_dinode_info->last_dinode_pointer = 0;
    new_dinode_info->user_id = statbuf.st_uid;
    new_dinode_info->group_id = statbuf.st_gid;
    new_dinode_info->file_state = file_state;
    new_dinode_info->file_mode = statbuf.st_mode;
    new_dinode_info->file_size = statbuf.st_size;
    new_dinode_info->mtime = statbuf.st_mtime;
    new_dinode_info->atime = statbuf.st_atime;
    new_dinode_info->ctime = statbuf.st_ctime;

    if ((new_dinode_info->file_mode & S_IFMT) == S_IFLNK) //symbolic link
        readlink(file, new_dinode_info->link_path, LINK_NAME_SIZE - 1);

    strncpy(new_dinode_info->file_dir_name, file, strlen(file) + 1);

    return new_dinode_info;
}


QueryAnswer DInodeInfo_query(DInodeInfo *dinode_info, char *file_name) {
    if (strcmp(dinode_info->file_dir_name, file_name) == 0)
        return yes;
    else
        return no;
}


void DInodeInfo_addEntry(DInodeInfo *dinode_info, char *entry, int entry_pointer, List dinode_list) {
    int dinode_id = dinode_info->dinode_pointers[dinode_info->last_dinode_pointer];

    DInode *di_node;
    if (dinode_id != -1)      //add an entry to an existing dinode
        di_node = List_find(dinode_list, dinode_id);
    else {                    //create a new dinode
        di_node = c_malloc(sizeof(DInode));
        di_node->last_entry = 0;         //last free entry of the dinode
        //pointer starts from zero
        //pointer is equal to the position of the dinode in the list
        dinode_info->dinode_pointers[dinode_info->last_dinode_pointer] = List_returnSize(dinode_list);
        List_addLast(dinode_list, di_node);
    }

    strcpy(di_node->entries[di_node->last_entry].name, entry);
    di_node->entries[di_node->last_entry].dinode_info_pointer = entry_pointer;
    di_node->last_entry++;
    if (di_node->last_entry == POINTERS_COUNT)       //dinode is full
        (dinode_info->last_dinode_pointer)++;                //move pointer to next block
}


int DInodeInfo_copyFile(DInodeInfo *dinode_info, int *start_block) {
    ssize_t nread ;
    int i = 0, fd;

    char compressed_file_name[FILE_NAME_SIZE + 10];
    if (dinode_info->file_state == compressed) {    //compressed file
        compressFile(dinode_info->file_dir_name);
        sprintf(compressed_file_name, "%s.gz", dinode_info->file_dir_name);
        struct stat statbuf;
        lstat(compressed_file_name, &statbuf);
        dinode_info->file_size = statbuf.st_size;
        fd = open(compressed_file_name, O_RDONLY);
    }
    else                                            //uncompressed file
        fd = open(dinode_info->file_dir_name, O_RDONLY);

    if (fd == -1)
        return ERROR;

    if (*start_block == 1)
        BF_allocateBlock();     //header block

    void *block;
    char buffer[BLOCK_SIZE];
    while ((nread = read(fd, buffer, BLOCK_SIZE)) > 0 ) {
        //create a new block if block doesn't already exist
        if (*start_block >= BF_getBlockCounter())
            BF_allocateBlock();

        dinode_info->dinode_pointers[i++] = *start_block;
        BF_readBlock(*start_block, &block);
        memcpy(block, buffer, nread);
        (*start_block)++;
    }
    close(fd);

    if (dinode_info->file_state == compressed)
        remove(compressed_file_name);

    return SUCCESS;
}


int DInodeInfo_extract(DInodeInfo *dinode_info) {
    //extract file from di-file
    int i, fd;
    char compressed_file_name[FILE_NAME_SIZE + 10];

    if (dinode_info->file_state == compressed) {        //compressed file
        sprintf(compressed_file_name, "%s.gz", dinode_info->file_dir_name);
        fd = open(compressed_file_name, O_CREAT | O_WRONLY, dinode_info->file_mode);
    }
    else                                                //uncompressed file
        fd = open(dinode_info->file_dir_name, O_CREAT | O_WRONLY, dinode_info->file_mode);

    if (fd == -1)
        return ERROR;

    off_t bytes_remaining = dinode_info->file_size;
    void *block;
    for (i = 0; dinode_info->dinode_pointers[i] != -1; i++) {
        BF_readBlock(dinode_info->dinode_pointers[i], &block);
        if (bytes_remaining >= BLOCK_SIZE)
            write(fd, block, BLOCK_SIZE);
        else
            write(fd, block, bytes_remaining);
        bytes_remaining -= BLOCK_SIZE;
    }
    close(fd);

    if (dinode_info->file_state == compressed) {
        decompressFile(compressed_file_name);
        remove(compressed_file_name);
    }
    return SUCCESS;
}


void DInodeInfo_extractDirectory(DInodeInfo *dinode_info, List dinode_info_list,
                                 List dinode_list, int *visited_nodes) {
    DInode *di_node;
    int i, j, dinode_id;

    mkdir(dinode_info->file_dir_name, dinode_info->file_mode);
    chdir(dinode_info->file_dir_name);
    //iterate through dinode pointers
    for (i = 0; i <= dinode_info->last_dinode_pointer; i++) {
        dinode_id = dinode_info->dinode_pointers[i];
        if (dinode_id == -1)
            break;
        //find i-th dinode
        di_node = List_find(dinode_list, dinode_id);

        DInodeInfo *dinode_info_entry;
        for (j = 0; j < di_node->last_entry; j++) {         //for each entry of the current dinode
            if (strcmp(di_node->entries[j].name, ".") == 0 || strcmp(di_node->entries[j].name, "..") == 0)
                continue;

            //find dinode_info for the current dinode entry
            dinode_info_entry = List_find(dinode_info_list, di_node->entries[j].dinode_info_pointer);
            visited_nodes[di_node->entries[j].dinode_info_pointer] = 1;

            if ((dinode_info_entry->file_mode & S_IFMT) == S_IFREG)       //file
                DInodeInfo_extract(dinode_info_entry);
            else if ((dinode_info_entry->file_mode & S_IFMT) == S_IFLNK)
                symlink(dinode_info_entry->link_path, dinode_info_entry->file_dir_name);
            else if ((dinode_info_entry->file_mode & S_IFMT) == S_IFDIR)      //directory
                DInodeInfo_extractDirectory(dinode_info_entry, dinode_info_list, dinode_list, visited_nodes);
        }
    }
    chdir("..");
}


void printIndented(char *name, int depth) {
    int i;
    for (i = 0; i < depth; i++)
        printf("   ");
    printf("%s\n", name);
}


void printDirHierarchy(DInodeInfo *dinode_info, List dinode_info_list, List dinode_list, int *visited_nodes) {
    static int depth = 1;
    DInode *di_node;
    int i, j, dinode_id;

    //iterate through dinode pointers
    for (i = 0; i <= dinode_info->last_dinode_pointer; i++) {
        dinode_id = dinode_info->dinode_pointers[i];
        if (dinode_id == -1)
            return;
        di_node = List_find(dinode_list, dinode_id);

        DInodeInfo *new_dinode_info;
        for (j = 0; j < di_node->last_entry; j++) {
            if (strcmp(di_node->entries[j].name, ".") == 0 || strcmp(di_node->entries[j].name, "..") == 0)
                continue;

            //find dinode_info for the current dinode entry
            new_dinode_info = List_find(dinode_info_list, di_node->entries[j].dinode_info_pointer);
            visited_nodes[di_node->entries[j].dinode_info_pointer] = 1;

            if ((new_dinode_info->file_mode & S_IFMT) == S_IFREG)       //file
                printIndented(new_dinode_info->file_dir_name, depth);
            else {                                                      //directory
                printIndented(new_dinode_info->file_dir_name, depth);
                depth++;
                printDirHierarchy(new_dinode_info, dinode_info_list, dinode_list, visited_nodes);
                depth--;
            }
        }
    }
}


void DInodeInfo_print(DInodeInfo dinode_info) {
    //print dinode_info metadata
    char 		type, perms[10];
    int 		i,j;

    struct passwd *pw = getpwuid(dinode_info.user_id);
    struct group *gr = getgrgid(dinode_info.group_id);

    switch (dinode_info.file_mode & S_IFMT) {
        case S_IFREG: type = '-'; break;
        case S_IFLNK: type = 'l'; break;
        case S_IFDIR: type = 'd'; break;
        default:      type = '?'; break;
    }

    *perms='\0';
    for(i=2; i>=0; i--){
        j = (dinode_info.file_mode >> (i*3)) & 07;
        strcat(perms, modes[j]);
    }

    char postfix[4];
    if (dinode_info.file_state == compressed && (dinode_info.file_mode & S_IFMT) == S_IFREG)
        strcpy(postfix, ".gz");
    else
        strcpy(postfix, "");
    printf("%c%s %5s/%-5s %7d %.12s %s%s\n",\
		type, perms, pw->pw_name, gr->gr_name, \
		(int)dinode_info.file_size, ctime(&dinode_info.mtime)+4, dinode_info.file_dir_name, postfix);
}


void DInode_print(DInodeInfo dinode_info, List dinode_list) {
    //print dinode entries
    if ((dinode_info.file_mode & S_IFMT) == S_IFDIR) {      //directory
        int i, j, dinode_id;
        DInode *di_node;
        Iterator it;
        List_initIterator(dinode_list, &it);
        printf("Dir: %s\n", dinode_info.file_dir_name);

        for (i = 0; i <= dinode_info.last_dinode_pointer; i++) {
            dinode_id = dinode_info.dinode_pointers[i];
            if (dinode_id == -1)
                return;
            di_node = List_find(dinode_list, dinode_id);

            for (j = 0; j < di_node->last_entry; j++)
                printf("%10s | %2d\n", di_node->entries[j].name, di_node->entries[j].dinode_info_pointer);
        }
        printf("\n");
    }
}


int compressFile(char *file) {
    int pid = fork();
    if (pid == 0) {
        if (execlp("gzip", "gzip", "-k", file, NULL) < 0)
            return ERROR;
    }
    else
        wait(NULL);

    return SUCCESS;
}


int decompressFile(char *file) {
    int pid = fork();
    if (pid == 0) {
        if (execlp("gzip", "gzip", "-d", file, NULL) < 0)
            return ERROR;
    }
    else
        wait(NULL);

    return SUCCESS;
}