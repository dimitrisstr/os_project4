#ifndef _UTILITIES_H
#define _UTILITIES_H

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include "../malloc/malloc.h"
#include "../block_structure/block_file.h"

#define SUCCESS 0
#define ERROR -1

#define POINTERS_COUNT 8
#define FILE_NAME_SIZE 32
#define LINK_NAME_SIZE 48
#define DIR_NAME_SIZE 64


typedef enum QueryAnswer {yes, no} QueryAnswer;

typedef enum FileState {compressed, uncompressed} FileState;

typedef struct DInodeEntry {
    char name[DIR_NAME_SIZE];
    int dinode_info_pointer;
} DInodeEntry;

typedef struct DInode {
    DInodeEntry entries[POINTERS_COUNT];
    int last_entry;
} DInode;

typedef struct DInodeInfo {
    char file_dir_name[FILE_NAME_SIZE];
    char link_path[LINK_NAME_SIZE];
    uid_t user_id;
    gid_t group_id;
    mode_t file_mode;       //file type and permissions
    off_t file_size;        //file size
    time_t mtime;           //time of last access
    time_t atime;           //time of last modification
    time_t ctime;           //time of last status change
    int dinode_pointers[POINTERS_COUNT];                //pointers to dinodes
    int last_dinode_pointer;
    FileState file_state;
} DInodeInfo;


DInodeInfo *DInodeInfo_create(char *file, FileState file_state);

QueryAnswer DInodeInfo_query(DInodeInfo *dinode_info, char *file_name);

void DInodeInfo_addEntry(DInodeInfo *dinode_info, char *entry, int entry_pointer, List dinode_list);

int DInodeInfo_copyFile(DInodeInfo *dinode_info, int *start_block);

int DInodeInfo_extract(DInodeInfo *dinode_info);

void DInodeInfo_extractDirectory(DInodeInfo *dinode_info, List dinode_info_list,
                                 List dinode_list, int *visited_nodes);

void DInodeInfo_print(DInodeInfo dinode_info);

void DInode_print(DInodeInfo dinode_info, List dinode_list);

void printDirHierarchy(DInodeInfo *dinode_info, List dinode_info_list, List dinode_list, int *visited_nodes);

#endif //_UTILITIES_H
