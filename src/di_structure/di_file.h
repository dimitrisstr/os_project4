#ifndef _METADATA_H
#define _METADATA_H

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include "dinode_info.h"
#include "../block_structure/block_file.h"
#include "../data_structures/list.h"
#include "../malloc/malloc.h"


#define SUCCESS 0
#define ERROR -1

typedef struct DIfile* DIfilePtr;


DIfilePtr DIfile_create();

int DIfile_add(DIfilePtr di_file, char *dir_file, int previous_dir_pointer, FileState file_state);

void DIfile_writeToFile(DIfilePtr di_file);

void DIfile_loadFromFile(DIfilePtr di_file);

void DIfile_printHierarchy(DIfilePtr di_file);

void DIfile_printMetadata(DIfilePtr di_file);

void DIfile_extractAll(DIfilePtr di_file);

QueryAnswer DIfile_query(DIfilePtr di_file, char *file);

void DIfile_destroy(DIfilePtr *di_file);

#endif //_METADATA_H
