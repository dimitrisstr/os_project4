#include <stdio.h>
#include <stdlib.h>
#include "malloc/malloc.h"
#include "di_structure/di_file.h"


typedef enum Operation {
    create,
    append,
    extract,
    compress,
    query,
    print_metadata,
    print_hierarchy,
    no_operation
} Operation;


void executionError() {
    fprintf(stderr, "usage: ./mydiz {-c|-a|-x|-m|-d|-p|-j} <archive-file> <list-of-files/dirs>\n");
    exit(EXIT_FAILURE);
}


int main(int argc, char  *argv[]) {
    BF_init();
    Operation operations[2];
    operations[0] = no_operation;
    operations[1] = no_operation;

    if (argc < 3)
        executionError();

    int i;
    //find first two flags
	for (i = 1; i < 3 ; i++) {
        if (strcmp(argv[i], "-c") == 0)
            operations[i - 1] = create;
        else if (strcmp(argv[i], "-a") == 0)
            operations[i - 1] = append;
        else if (strcmp(argv[i], "-x") == 0)
            operations[i - 1] = extract;
        else if (strcmp(argv[i], "-j") == 0)
            operations[i - 1] = compress;
        else if (strcmp(argv[i], "-m") == 0)
            operations[i - 1] = print_metadata;
        else if (strcmp(argv[i], "-q") == 0)
            operations[i - 1] = query;
        else if (strcmp(argv[i], "-p") == 0)
            operations[i - 1] = print_hierarchy;
	}

    if (operations[0] == no_operation && operations[1] == no_operation)     //no valid flags found
        executionError();

    char *archive_file;
    if (operations[1] == no_operation) {        //one flag found
        i = 3;
        archive_file = argv[2];
    }
    else {                                      //two flags found
        i = 4;
        archive_file = argv[3];
    }

    DIfilePtr di_file;
    FileState file_state = uncompressed;
    if (operations[0] == create || operations[1] == create) {
        char *difile_name = c_malloc(sizeof(char) * (strlen(archive_file) + 5));
        sprintf(difile_name, "%s.di", archive_file);
        BF_createFile(difile_name);
        di_file = DIfile_create();
        free(difile_name);
    }
    else {
        if (BF_openFile(archive_file) == BFE_OPENFILE) {
            fprintf(stderr, "Error: %s: file not found!", archive_file);
            exit(EXIT_FAILURE);
        }

        di_file = DIfile_create();
        DIfile_loadFromFile(di_file);
    }

    if (operations[0] == print_metadata) {
        DIfile_printMetadata(di_file);
    }
    else if (operations[0] == print_hierarchy) {
        DIfile_printHierarchy(di_file);
    }
    else if (operations[0] == extract) {
        DIfile_extractAll(di_file);
    }
    else if (operations[0] == compress || operations[1] == compress)
        file_state = compressed;

    for ( ; i < argc; i++) {
        if (operations[0] == append || operations[1] == append || operations[0] == create
                || operations[1] == create)
            DIfile_add(di_file, argv[i], -1, file_state);
        else if (operations[0] == query || operations[1] == query) {
            if (DIfile_query(di_file, argv[i]) == yes)
                printf("%s: yes\n", argv[i]);
            else
                printf("%s: no\n", argv[i]);
        }
    }

    if (operations[0] == create || operations[1] == create || operations[0] == append || operations[1] == append)
        DIfile_writeToFile(di_file);
    DIfile_destroy(&di_file);
    BF_close();

	exit(EXIT_SUCCESS);
}