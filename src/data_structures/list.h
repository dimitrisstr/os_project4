#ifndef LIST_H
#define LIST_H

#include "../malloc/malloc.h"

typedef struct infoNode* List;

typedef struct listNode* Iterator;

int List_create(List *lst);

int List_addLast(List lst, void *data);

void *List_remove(List lst, void *cvalue, int (*Compare)(void *, void *));

void *List_removeFirst(List lst);

void *List_removeLast(List lst);

void* List_find(List lst, int position);

int List_destroy(List *lst);

int List_isEmpty(List lst);

int List_returnSize(List lst);

void* List_returnValue(Iterator *it);

void List_initIterator(List lst, Iterator *it);

#endif /* LIST_H */
