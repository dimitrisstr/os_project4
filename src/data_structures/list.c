#include "list.h"


//data node
typedef struct listNode
{
    void *data;
    struct listNode* next;
} listNode;

//information node
typedef struct infoNode
{
    int size;
    listNode* start;
    listNode* last;
} infoNode;



int List_create(List *lst) {
    //allocate a new infoNode
    *lst = c_malloc(sizeof(infoNode));

    //initialize variables
    (*lst)->size = 0;
    (*lst)->start = NULL;
    (*lst)->last = NULL;
    return 1;
}


void *List_remove(List lst, void *cvalue, int (*Compare)(void *, void *)) {
	listNode *previous_node, *lnode = lst->start;
	if ((*Compare)(cvalue, lnode->data) == 0)
		return List_removeFirst(lst);
	else {
		previous_node = lnode;
		lnode = lnode->next;
		while (lnode != NULL) {
			if ((*Compare)(cvalue, lnode->data) == 0) {
				previous_node->next = lnode->next;
				if (lst->last == lnode)
					lst->last = previous_node;
				void *node_data = lnode->data;
				free(lnode);
				lst->size--;
				return node_data;
			}
			previous_node = lnode;
			lnode = lnode->next;
		}
	}
	return NULL;
}


int List_addLast(List lst, void *data) {
    //add a node a the end of the list
    listNode *newNode = c_malloc(sizeof(listNode));

    newNode->data = data;
    newNode->next = NULL;
    if(lst->last == NULL) {
        lst->start = newNode;
        lst->last = newNode;
    }
    else {
        lst->last->next = newNode;
        lst->last = newNode;
    }
    lst->size++;
    return 0;
}


void* List_find(List lst, int position) {
    //position starts from zero
    Iterator it;
    List_initIterator(lst, &it);
    void *value;
    int counter = 0;
    while ((value = List_returnValue(&it)) != NULL) {
        if (counter++ == position)
            return value;
    }
    return NULL;
}


void* List_removeFirst(List lst) {
    /*
     *remove the first node of the list
     *free ONLY the memory allocated
     *for the node and not the data
     *return the data pointer
     */
    if(lst->start == NULL)
        return NULL;
    else {
        listNode *temp;
        void *value;
        temp = lst->start;
        lst->start = temp->next;
        value = temp->data;
        free(temp);
        lst->size--;
        return value;
    }
}


void *List_removeLast(List lst) {
	void *returnValue;
	if(lst->start == NULL)
		return NULL;
	else if(lst->start == lst->last) {
		returnValue = lst->start->data;
		free(lst->last);
		lst->start = NULL;
		lst->last = NULL;
		lst->size--;
	}
	else {
		int i;
		listNode *temp = lst->start;
		for(i = 0; i < lst->size - 2; i++)
			temp = temp->next;
		returnValue = lst->last->data;
		free(lst->last);
		lst->last = temp;
		temp->next = NULL;
		lst->size--;
	}
	return returnValue;
}


int List_destroy(List *lst) {
	//destroy list
	listNode *temp1 = (*lst)->start;
	listNode *temp2;
	while(temp1 != NULL)
	{
		temp2 = temp1;
		temp1 = temp1->next;
		free(temp2->data);
		free(temp2);
	}
	free(*lst);
	return 0;
}


void List_initIterator(List lst, Iterator *it) {
    //initialize the iterator to point at the start of the list
    *it = lst->start;
}


int List_isEmpty(List lst) {
	return lst->size == 0;
}


int List_returnSize(List lst) {
    //return the size of the list
    return lst->size;
}


void* List_returnValue(Iterator *it) {
    /*
     * return the data of the node that the iterator
     * points to and move the iterator to the next node
     */
    void *data;
    if (*it != NULL) {
        data = (*it)->data;
        *it = (*it)->next;
        return data;
    }
    return NULL;
}
